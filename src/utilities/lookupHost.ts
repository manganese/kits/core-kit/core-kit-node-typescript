import { lookup } from 'node:dns';

const IPV4_REGEX = /^((25[0-5]|(2[0-4]|1\d|[1-9]|)\d)\.?\b){4}$/;

export default async (host: string) => {
  if (host.match(IPV4_REGEX)) {
    return host;
  } else if (host) {
    return new Promise<string>((resolve, reject) => {
      lookup(host, (error, ip: string) => {
        if (error) {
          reject(error);

          return;
        } else if (!ip || (Array.isArray(ip) && ip.length < 1)) {
          reject();

          return;
        }

        resolve(ip);
      });
    });
  } else {
    return null;
  }
};
